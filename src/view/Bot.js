import React, {Component} from 'react';

class Bot extends Component {
    render() {
        const getColor = (bot) => {
            if (bot === this.props.activeBot) return "has-text-success";
            if (!bot.isOnGrid) return "has-text-danger";
            return "has-text-warning";
        };
        const bots = (this.props.bots.map((bot, idx) => (
            <span key={idx}>
                <i className={`fa fa-android ${getColor(bot)}`}/>
                {bot.position.orientation.orientation}
            </span>)));
        return (
            <div className="column">
                <span className="icon is-medium">
                    {bots}
                    [{this.props.x},{this.props.y}]
                </span>
            </div>
        );
    }
}

export default Bot;