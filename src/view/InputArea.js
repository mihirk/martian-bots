import React from 'react';

export default ({inputCommands, handleInput, handleClick, validation: {error, errorMessage}}) => (
    <div className="box">
        <div className="input-area">
            <div className="field">
                <div className="control">
                            <textarea
                                value={inputCommands}
                                onChange={handleInput}
                                className={`textarea ${error ? "is-danger" : ""}`}
                                placeholder="Please enter your input commands"/>
                </div>
                {error ? (<p className="help is-danger">{errorMessage}</p>) : (<span/>)}
            </div>
            <div className="field has-addons has-addons-centered">
                <div className="control">
                    <button className="button is-dark is-large" onClick={handleClick}>Play</button>
                </div>
            </div>
        </div>
    </div>);