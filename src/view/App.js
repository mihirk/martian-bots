import React, {Component} from 'react';
import logo from '../imgs/bot.png';
import '../styles/App.css';
import '../styles/lib/bulma.css';
import InputArea from './InputArea';
import MarsGrid from "../model/MarsGrid";
import InputParser from "../input/InputParser";
import Grid from "./Grid";

class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            inputCommands: "5 3\n" +
            "1 1 E\n" +
            "RFRFRFRF\n" +
            "3 2 N\n" +
            "FRRFLLFFRRFLL\n" +
            "0 3 W\n" +
            "LLFFFLFLFL",
            validation: {error: false},
            grid: undefined
        };
        this.marsGrid = undefined;
    }

    handleInput(event) {
        this.setState({inputCommands: event.target.value});
    }

    handleClick() {
        try {
            const parsedInput = new InputParser(this.state.inputCommands).parse();
            this.marsGrid = MarsGrid.factory(parsedInput);
            this.setState({validation: {error: false}, grid: this.marsGrid.viewModel});
        } catch (error) {
            this.setState({validation: {error: true, errorMessage: error.message}});
        }
    }

    newGame() {
        this.setState({grid: undefined}, () => this.marsGrid = undefined);
    }

    handleStep() {
        this.state.grid.step();
        this.setState({grid: this.marsGrid.viewModel});
    }

    handleStepAll() {
        this.state.grid.stepAll();
        this.setState({grid: this.marsGrid.viewModel});
    }

    render() {
        const Body = this.state.grid === undefined ? (<InputArea
                validation={this.state.validation}
                handleInput={this.handleInput.bind(this)}
                inputCommands={this.state.inputCommands}
                handleClick={this.handleClick.bind(this)}/>) :
            (<Grid input={this.state.grid}
                   handleStep={this.handleStep.bind(this)}
                   handleStepAll={this.handleStepAll.bind(this)}
                   newGame={this.newGame.bind(this)}/>);

        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <div className="App-intro">
                    {Body}
                </div>
            </div>
        );
    }
}

export default App;
