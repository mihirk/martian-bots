import React, {Component} from 'react';

class GridPoint extends Component {
    render() {
        return (
            <div className="column">
            <span className="icon is-medium">
                <i className={`fa fa-circle-o`}/>
                [{this.props.x},{this.props.y}]
            </span>
            </div>
        );
    }
}

export default GridPoint;