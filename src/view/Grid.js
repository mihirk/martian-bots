import React, {Component} from 'react';
import '../styles/App.css';
import '../styles/lib/bulma.css';
import transpose from '../util/transpose';
import GridPoint from "./GridPoint";
import Bot from "./Bot";

class Grid extends Component {
    renderOutput() {
        return (
            <div className="output">
                Output
                {this.props.input.bots.map((bot, idx) => <p key={idx}>{bot.finalPosition()}</p>)}
                <button className={`button is-dark is-large`}
                        onClick={this.props.newGame}>
                    Play Again
                </button>
            </div>);
    }

    renderButtons() {
        return (
            <div className="field has-addons has-addons-centered">
                <div className="control">
                    <div className="columns">
                        <div className="column">
                            <button className={`button is-dark is-large`}
                                    onClick={this.props.handleStep}>
                                Take 1-Step
                            </button>
                        </div>
                        <div className="column">
                            <button className={`button is-dark is-large`}
                                    onClick={this.props.handleStepAll}>
                                Step All
                            </button>
                        </div>
                    </div>
                </div>
            </div>);
    }

    renderControl(gameIsOnGoing) {
        return gameIsOnGoing ? this.renderButtons() : this.renderOutput();

    }

    render() {
        const gameIsOnGoing = this.props.input.activeBot !== undefined;
        const botsAt = (x, y) => this.props.input.bots.filter(bot => bot.isAt(x, y));
        const pointToRender = (k) => new Map([[true, Bot], [false, GridPoint]]).get(k);
        const icons = transpose(this.props.input.grid).map((col, y) =>
            (<div key={y} className="columns">
                {col.map((row, x) => {
                        const bots = botsAt(x, y);
                        const Point = pointToRender(bots.length > 0);
                        return (<Point key={`${x}${y}`}
                                       x={x}
                                       y={y}
                                       bots={bots}
                                       activeBot={this.props.input.activeBot}/>);
                    }
                )}
            </div>));
        return (
            <div className="Grid">
                <div className="box">
                    <div className="container">
                        {icons.reverse()}
                    </div>
                </div>
                <div className="box">
                    {this.renderControl(gameIsOnGoing)}
                </div>
            </div>
        );
    }
}

export default Grid;
