import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './view/App';
import registerServiceWorker from './worker/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
