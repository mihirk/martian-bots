export default class Orientation {
    constructor(orientation) {
        this.orientation = orientation;
    }

    rotateBy(degrees, orientationMap, inverseOrientationMap) {
        const newDegreeOrientation = (360 + (orientationMap[this.orientation] + degrees)) % 360;
        return new Orientation(inverseOrientationMap[newDegreeOrientation]);
    }
}