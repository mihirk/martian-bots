import Coordinates from './Coordinates';
import GridPoint from './GridPoint';
import Bot from "./Bot";

export default class MarsGrid {
    constructor(bots, topRightCorner, bottomLeftCorner = Coordinates.Origin) {
        this.botMap = bots.reduce((allBots, bot) => allBots.set(bot.id, bot), new Map());
        this.length = topRightCorner.x - bottomLeftCorner.x;
        this.width = topRightCorner.y - bottomLeftCorner.y;
        this.grid = [...new Array(this.length + 1)]
            .map((_) => [...new Array(this.width + 1)]
                .map((_) => new GridPoint()));
    }

    get bots() {
        return Array.from(this.botMap.values())
    }

    get activeBot() {
        return this.bots.find(bot => bot.isActive);
    }

    get smellyCoordinates() {
        return this.bots
            .filter((bot) => !bot.isOnGrid)
            .map((bot) => bot.position.coordinates);
    }

    get viewModel() {
        return {
            grid: this.grid,
            bots: this.bots,
            step: this.step.bind(this),
            stepAll: this.stepAll.bind(this),
            isOnGrid: this.isOnGrid.bind(this),
            activeBot: this.activeBot,
        }
    }

    isOnGrid(position) {
        return this.grid[position.coordinates.x] !== undefined &&
            this.grid[position.coordinates.x][position.coordinates.y] !== undefined;
    }

    step() {
        const activeBot = this.activeBot;
        if (activeBot === undefined) return false;
        this.botMap.set(activeBot.id, activeBot.move(this.isOnGrid.bind(this), this.smellyCoordinates));
        return true;
    }

    stepAll() {
        while (this.step()) {
        }
    }


    static factory({topRightCoordinates, bots}) {
        return new MarsGrid(bots.map(Bot.factory), topRightCoordinates, Coordinates.Origin);
    }
}