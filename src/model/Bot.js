import head from "../util/head";
import Position from './Position';
import Command from './Command';
import tail from "../util/tail";

export default class Bot {
    constructor(id, position, commands, isOnGrid = true) {
        this.id = id;
        this.position = position;
        this.commands = commands;
        this.isOnGrid = isOnGrid;
    }

    get isActive() {
        return this.commands.length > 0 && this.isOnGrid;
    }

    move(gridCheck, smellyCoordinates) {
        const newPosition = head(this.commands).exec(this.position);
        const isOffGrid = !gridCheck(newPosition);
        const ignoreCommand = isOffGrid && this.position.in(smellyCoordinates);
        const updatedPosition = isOffGrid || ignoreCommand ? this.position : newPosition;
        return new Bot(this.id, updatedPosition, tail(this.commands), !isOffGrid || ignoreCommand);
    }

    isAt(x, y) {
        return this.position.coordinates.x === x && this.position.coordinates.y === y
    }

    finalPosition() {
        return `${this.position.coordinates.x} ${this.position.coordinates.y} ${this.position.orientation.orientation} ${this.isOnGrid ? "" : "LOST"}`.trim();
    }

    static factory({position, commands}, id) {
        return new Bot(id, Position.factory(position), commands.map((command) => new Command(command)));
    }
}