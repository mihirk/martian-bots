import CommandActionMap from './CommandActionMap';

export default class Command {
    constructor(command) {
        this.command = command;
    }

    exec(currentPosition) {
        const commandDef = CommandActionMap[this.command];
        return currentPosition.update(commandDef);
    }

}