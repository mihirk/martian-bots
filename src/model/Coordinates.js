export default class Coordinates {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.addX = this.updateX(1);
        this.subX = this.updateX(-1);
        this.addY = this.updateY(1);
        this.subY = this.updateY(-1);
    }

    updateX(sign) {
        return (updateVal) => new Coordinates(this.x + (updateVal * sign), this.y);
    }

    updateY(sign) {
        return (updateVal) => new Coordinates(this.x, this.y + (updateVal * sign));
    }

    eq(coordinates) {
        return this.x === coordinates.x && this.y === coordinates.y;
    }

    static get Origin() {
        return new Coordinates(0, 0);
    }
}