export default {
    "L": {
        degrees: +90,
        step: 0,
    },
    "R": {
        degrees: -90,
        step: 0,
    },
    "F": {
        degrees: 0,
        step: 1,
    }
}