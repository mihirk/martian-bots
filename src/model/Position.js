import Coordinates from "./Coordinates";
import Orientation from "./Orientation";

export default class Position {
    constructor(coordinates, orientation) {
        this.coordinates = coordinates;
        this.orientation = orientation;
    }

    get orientationMap() {
        return {
            "E": {deg: 0, coord: this.coordinates.addX},
            "N": {deg: 90, coord: this.coordinates.addY},
            "W": {deg: 180, coord: this.coordinates.subX},
            "S": {deg: 270, coord: this.coordinates.subY}
        };
    };

    get orientationDegreeMap() {
        return Object.keys(this.orientationMap)
            .reduce((acc, k) => {
                acc[k] = this.orientationMap[k].deg;
                return acc
            }, {});
    };

    get inverseOrientationMap() {
        return Object.keys(this.orientationMap)
            .reduce((acc, k) => {
                acc[this.orientationMap[k].deg] = k;
                return acc;
            }, {});
    };

    update({degrees, step}) {
        const orientation = this.orientation.rotateBy(degrees, this.orientationDegreeMap, this.inverseOrientationMap);
        const coordinates = this.orientationMap[orientation.orientation].coord(step);
        return new Position(coordinates, orientation);
    }

    in(coordinatesList) {
        return coordinatesList.some((coord) => coord.eq(this.coordinates));
    }

    static factory({x, y, orientation}) {
        return new Position(new Coordinates(x, y), new Orientation(orientation));
    }
}