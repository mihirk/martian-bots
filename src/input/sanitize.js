export const removeEmptyLines = (str) => str.replace(/^[\s\n]*$/gm, "");
export const trimAllLines = (str) => str.trim().split("\n").map((strL) => strL.trim()).join("\n");