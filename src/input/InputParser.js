import ValidationError from "../error/ValidationError";
import {blankString, checkUndefined, patternUnMatch} from "./validate";
import tail from '../util/tail';
import head from "../util/head";
import chunk from "../util/chunk";
import {removeEmptyLines, trimAllLines} from "./sanitize";

export default class InputParser {
    constructor(str) {
        this.inputString = str;
        this.topRightCoordinatesPattern = "([0-9]+)\\s+([0-9]+)[\\s]*[\\n]*";
        this.botPositionPattern = "([0-9]+)\\s+([0-9]+)\\s+([NSEW])[\\s\\n]*";
        this.botCommandPattern = `([RLF ]+)[\\s\\n]*`;
        this.parse = this.parse.bind(this);
        this.validate = this.validate.bind(this);
        this.sanitize = this.sanitize.bind(this);
    }

    get VALID_PATTERN() {
        return new RegExp(`${this.topRightCoordinatesPattern}(${this.botPositionPattern}${this.botCommandPattern})+`,
            "gm");
    }

    topRightCoordinates(str) {
        const rawCoordinates = new RegExp(this.topRightCoordinatesPattern, "g").exec(str);
        return {x: parseInt(rawCoordinates[1], 10), y: parseInt(rawCoordinates[2], 10)};
    }

    botPosition(strLines) {
        const rawPosition = new RegExp(this.botPositionPattern, "g").exec(strLines);
        return {x: parseInt(rawPosition[1], 10), y: parseInt(rawPosition[2], 10), orientation: rawPosition[3]};
    }

    commands(strLines) {
        const rawCommands = new RegExp(`^${this.botCommandPattern}$`, "gm").exec(strLines)[1];
        return rawCommands.split("").filter((cmd) => cmd.trim() !== "");
    }

    validate() {
        const failedValidation = [checkUndefined, blankString, patternUnMatch(this.VALID_PATTERN)]
            .find(fn => fn(this.inputString).test);
        if (failedValidation !== undefined) throw new ValidationError(failedValidation(this.inputString).error);
    }

    sanitize() {
        this.inputString = [trimAllLines, removeEmptyLines]
            .reduce((cleanerStr, fn) => fn(cleanerStr), this.inputString);
    }

    parse() {
        [this.validate, this.sanitize].forEach((fn) => fn());
        const inputLines = this.inputString.split("\n").filter(line => line !== "");
        const topRightCoordinates = this.topRightCoordinates(head(inputLines));
        const bots = chunk(tail(inputLines), 2)
            .map(([position, commands]) =>
                Object.assign({position: this.botPosition(position), commands: this.commands(commands)}));
        return {
            topRightCoordinates: topRightCoordinates,
            bots: bots
        }
    }
};