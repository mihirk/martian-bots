const make = (test, error) => Object.assign({test: test, error: `${error} Input`});

export const checkUndefined = (str) => make(str === undefined || str === null, "Undefined");

export const blankString = (str) => make(str.trim() === "", "Blank");

export const patternUnMatch = (pattern) => (str) => make(!pattern.exec(str), "Malformed");