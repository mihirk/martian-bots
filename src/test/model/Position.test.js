import Position from '../../model/Position';
import Coordinates from '../../model/Coordinates';


describe("Position Test", () => {
    describe("Update", () => {
        it("Shouldn't update coordinates if step is 0", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 0, step: 0});
            expect(position.coordinates.x).toBe(1);
            expect(position.coordinates.y).toBe(1);
        });

        it("Shouldn't update direction if degree is 0", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 0, step: 0});
            expect(position.orientation.orientation).toBe("E");
        });

        it("Should add 1 to x coordinate if step is 1 in same direction", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 0, step: 1});
            expect(position.coordinates.x).toBe(2);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("E");
        });

        it("Should subtract 1 from x coordinate and update orientation if step is 1 in opposite", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 180, step: 1});
            expect(position.coordinates.x).toBe(0);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("W");
        });

        it("Should subtract 1 from x coordinate and not update orientation if step is -1", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 0, step: -1});
            expect(position.coordinates.x).toBe(0);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("E");
        });

        it("Should update Y coordinate by 1 if step is 1 in 90 degrees", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: 90, step: 1});
            expect(position.coordinates.x).toBe(1);
            expect(position.coordinates.y).toBe(2);
            expect(position.orientation.orientation).toBe("N");
        });

        it("Should update Y coordinate by 1 if step is 1 in -90 degrees", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = position.update({degrees: -90, step: 1});
            expect(position.coordinates.x).toBe(1);
            expect(position.coordinates.y).toBe(0);
            expect(position.orientation.orientation).toBe("S");
        });
    });

    describe("In", () => {
        const makeCoordList = (list) => list.map(([x, y]) => new Coordinates(x, y));

        it("Should return false if list is empty", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            let result = position.in([]);
            expect(result).toBeFalsy();
        });

        it("Should return false if coordinates do not exist in the list", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            let result = position.in(makeCoordList([[1, 2], [0, 2], [0, 0]]));
            expect(result).toBeFalsy();
        });

        it("Should return true if coordinates do exist in the list", () => {
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            let result = position.in(makeCoordList([[1, 2], [0, 2], [0, 0], [1, 1]]));
            expect(result).toBeTruthy();
        });
    });
});