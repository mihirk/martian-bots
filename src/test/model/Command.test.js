import Command from '../../model/Command';
import Position from '../../model/Position';

describe("Command Test", () => {
    describe("Test command exec", () => {
        it("L", () => {
            let L = new Command("L");
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = L.exec(position);
            expect(position.coordinates.x).toBe(1);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("N");
        });
        it("R", () => {
            let R = new Command("R");
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = R.exec(position);
            expect(position.coordinates.x).toBe(1);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("S");
        });

        it("F", () => {
            let F = new Command("F");
            let position = Position.factory({x: 1, y: 1, orientation: "E"});
            position = F.exec(position);
            expect(position.coordinates.x).toBe(2);
            expect(position.coordinates.y).toBe(1);
            expect(position.orientation.orientation).toBe("E");
        });
    });
});
