import InputParser from '../../input/InputParser';
import MarsGrid from '../../model/MarsGrid';

const validInput = "5 3\n" +
    "1 1 E\n" +
    "RFRFRFRF\n" +
    "\n" +
    "3 2 N\n" +
    "FRRFLLFFRRFLL\n" +
    "\n" +
    "0 3 W\n" +
    "LLFFFLFLFL";

describe("Mars Grid", () => {
    describe("Step By Step Play", () => {
        it("Should update position of the active bot for a single step", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            marsGrid.step();
            expect(marsGrid.activeBot.position.orientation.orientation).toBe("S");
            expect(marsGrid.activeBot.position.coordinates.x).toBe(1);
            expect(marsGrid.activeBot.position.coordinates.y).toBe(1);
        });

        it("Should update position of the active bot for a double step", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            marsGrid.step();
            marsGrid.step();
            expect(marsGrid.activeBot.position.orientation.orientation).toBe("S");
            expect(marsGrid.activeBot.position.coordinates.x).toBe(1);
            expect(marsGrid.activeBot.position.coordinates.y).toBe(0);
        });

        it("Should switch to next bot if commands for the current bot are over", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            "RFRFRFRF".split("").forEach(() => marsGrid.step());
            expect(marsGrid.activeBot.position.orientation.orientation).toBe("N");
            expect(marsGrid.activeBot.position.coordinates.x).toBe(3);
            expect(marsGrid.activeBot.position.coordinates.y).toBe(2);
        });

        it("Should switch to next bot if the current one falls off grid", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            "RFRFRFRFFRRFLLFF".split("").forEach(() => marsGrid.step());
            expect(marsGrid.activeBot.position.orientation.orientation).toBe("W");
            expect(marsGrid.activeBot.position.coordinates.x).toBe(0);
            expect(marsGrid.activeBot.position.coordinates.y).toBe(3);
        });

        it("Should ignore command to fall off grid if smelly", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            "RFRFRFRFFRRFLLFFLLFFFLF".split("").forEach(() => marsGrid.step());
            expect(marsGrid.activeBot.position.orientation.orientation).toBe("N");
            expect(marsGrid.activeBot.position.coordinates.x).toBe(3);
            expect(marsGrid.activeBot.position.coordinates.y).toBe(3);
        });

        it("Should preserve all bot positions", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            "RFRFRFRFFRRFLLFFLLFFFLFLFL".split("").forEach(() => marsGrid.step());
            const [bot1, bot2, bot3] = marsGrid.bots;
            expect(bot1.position.orientation.orientation).toBe("E");
            expect(bot1.position.coordinates.x).toBe(1);
            expect(bot1.position.coordinates.y).toBe(1);
            expect(bot2.position.orientation.orientation).toBe("N");
            expect(bot2.position.coordinates.x).toBe(3);
            expect(bot2.position.coordinates.y).toBe(3);
            expect(bot3.position.orientation.orientation).toBe("S");
            expect(bot3.position.coordinates.x).toBe(2);
            expect(bot3.position.coordinates.y).toBe(3);
        });

        it("Shouldn't step further if all commands have been executed", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            "RFRFRFRFFRRFLLFFLLFFFLFLFL".split("").forEach(() => expect(marsGrid.step()).toBeTruthy());
            expect(marsGrid.step()).toBeFalsy();
        });
    });

    describe("Step All", () => {
        it("Should Step through all the commands in one go sequentially", () => {
            const parsedInput = new InputParser(validInput).parse();
            const marsGrid = MarsGrid.factory(parsedInput);
            marsGrid.stepAll();
            const [bot1, bot2, bot3] = marsGrid.bots;
            expect(bot1.position.orientation.orientation).toBe("E");
            expect(bot1.position.coordinates.x).toBe(1);
            expect(bot1.position.coordinates.y).toBe(1);
            expect(bot2.position.orientation.orientation).toBe("N");
            expect(bot2.position.coordinates.x).toBe(3);
            expect(bot2.position.coordinates.y).toBe(3);
            expect(bot3.position.orientation.orientation).toBe("S");
            expect(bot3.position.coordinates.x).toBe(2);
            expect(bot3.position.coordinates.y).toBe(3);
        });
    });
});
