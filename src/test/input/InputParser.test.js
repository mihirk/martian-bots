import InputParser from '../../input/InputParser';
import ValidationError from "../../error/ValidationError";


const malformedInput = "1 1 E\n" +
    "RFRFRFRF\n" +
    "\n" +
    "3 2 N\n" +
    "FRRFLLFFRRFLL\n" +
    "\n" +
    "0 3 W\n" +
    "LLFFFLFLFL";

const validInput = "5 3\n" +
    "1 1 E\n" +
    "RFRFRFRF\n" +
    "\n" +
    "3 2 N\n" +
    "FRRFLLFFRRFLL\n" +
    "\n" +
    "0 3 W\n" +
    "LLFFFLFLFL";

const botInput1 = "5 3\n" +
    "1 1 E\n" +
    "RFRFRFRF";

const bot1InputSpacesAndNewLines = "5   3\n\n\n" +
    "1   1   E\n\n\n" +
    "R F           R     FRF         RF\n\n";


describe("Input Parsing", () => {
    describe("Input Validation", () => {
        it("Should throw a validation exception if input is undefined", () => {
            const undefinedInput = new InputParser(undefined);
            expect(undefinedInput.parse).toThrowError(ValidationError);
            expect(undefinedInput.parse).toThrowError("Undefined Input");
        });

        it("Should throw a validation exception if input is blank", () => {
            const blankInput = new InputParser("");
            expect(blankInput.parse).toThrowError(ValidationError);
            expect(blankInput.parse).toThrowError("Blank Input");
        });

        it("Should throw a validation exception if input is empty characters", () => {
            const emptyInput = new InputParser("           ");
            expect(emptyInput.parse).toThrowError(ValidationError);
            expect(emptyInput.parse).toThrowError("Blank Input");
        });

        it("Should throw a validation exception if input is malformed", () => {
            const malformedInput1 = new InputParser(malformedInput);
            const malformedInput2 = new InputParser("asndknasnk");
            const malformedInput3 = new InputParser("5 3\naknskanaknsd");
            const malformedInput4 = new InputParser("asndnaskcub5 3\naknskanaknsd");
            [malformedInput1, malformedInput2, malformedInput3, malformedInput4]
                .map((mi) => {
                    expect(mi.parse).toThrowError(ValidationError);
                    expect(mi.parse).toThrowError("Malformed Input");
                })

        });

        it("Shouldn't throw a validation exception if input is valid", () => {
            const emptyInput = new InputParser(validInput);
            expect(emptyInput.parse).not.toThrowError(ValidationError);
        });
    });

    describe("Input -> JS Object", () => {
        it("Should return the top right corner coordinates", () => {
            const {topRightCoordinates: {x, y}} = (new InputParser(validInput)).parse();
            expect(x).toBe(5);
            expect(y).toBe(3);
        });

        it("Should return position of 1 bot", () => {
            const {bots: [bot1]} = new InputParser(botInput1).parse();
            expect(bot1.position.x).toBe(1);
            expect(bot1.position.x).toBe(1);
            expect(bot1.position.orientation).toBe("E");
        });

        it("Should return commands as list of 1 bot", () => {
            const {bots: [bot1]} = new InputParser(botInput1).parse();
            expect(bot1.commands).toEqual(["R", "F", "R", "F", "R", "F", "R", "F"]);
        });

        it("Should return top right corner coordinates, position, commands despite extra spaces and new lines", () => {
            const {topRightCoordinates: {x, y}, bots: [bot1]} = new InputParser(bot1InputSpacesAndNewLines).parse();
            expect(x).toBe(5);
            expect(y).toBe(3);
            expect(bot1.position.x).toBe(1);
            expect(bot1.position.y).toBe(1);
            expect(bot1.position.orientation).toBe("E");
            expect(bot1.commands).toEqual(["R", "F", "R", "F", "R", "F", "R", "F"]);
        });

        it("Should return top right corner coordinates, position, commands for multiple bots", () => {
            const {topRightCoordinates: {x, y}, bots: [bot1, bot2, bot3]} = new InputParser(validInput).parse();
            expect(x).toBe(5);
            expect(y).toBe(3);
            expect(bot1.position.x).toBe(1);
            expect(bot1.position.y).toBe(1);
            expect(bot1.position.orientation).toBe("E");
            expect(bot1.commands).toEqual(["R", "F", "R", "F", "R", "F", "R", "F"]);
            expect(bot2.position.x).toBe(3);
            expect(bot2.position.y).toBe(2);
            expect(bot2.position.orientation).toBe("N");
            expect(bot2.commands).toEqual("FRRFLLFFRRFLL".split(""));
            expect(bot3.position.x).toBe(0);
            expect(bot3.position.y).toBe(3);
            expect(bot3.position.orientation).toBe("W");
            expect(bot3.commands).toEqual("LLFFFLFLFL".split(""));
        });
    });
});