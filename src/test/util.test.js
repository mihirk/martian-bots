import tail from '../util/tail';
import head from "../util/head";
import chunk from "../util/chunk";

describe("All Utils", () => {
    describe("Tail Array", () => {
        it("Should return array tail", () => {
            expect(tail([1, 2, 3, 4, 5, 6, 7, 8])).toEqual([2, 3, 4, 5, 6, 7, 8]);
        });

        it("Should return empty array for empty array", () => {
            expect(tail([])).toEqual([]);
        });
    });

    describe("Head Array", () => {
        it("Should return array head if it exists", () => {
            expect(head([1, 2, 3, 4, 5, 6, 7, 8])).toEqual(1);
        });
        it("Should return default value if empty array", () => {
            expect(head([], 3)).toEqual(3);
        });
        it("Should return undefined value if empty array and no default value given", () => {
            expect(head([])).toBeUndefined();
        })
    });

    describe("Chunk Collections", () => {
        it("Should be able to chunk the array in pairs", () => {
            expect(chunk([1, 2, 3, 4, 5, 6, 7, 8], 2)).toEqual([[1, 2], [3, 4], [5, 6], [7, 8]]);
        });
        it("Should be able to chunk the array in groups of 3", () => {
            expect(chunk([1, 2, 3, 4, 5, 6, 7, 8, 9], 3)).toEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
        })
    });
});
