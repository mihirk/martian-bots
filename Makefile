.PHONY: dev install run test build run-build deploy

install:
	npm install

dev:
	npm start

test:
	CI=true npm test

build:
	rm -rf build
	mkdir -p build
	npm run build

run-build: build
	cd build; python -m SimpleHTTPServer

run: dev

start: run

deploy:
	rm -rf public/
	cp -rf build public