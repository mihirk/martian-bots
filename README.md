# Martian Bots

#TL;DR

[Martian Bots](https://mihirk.gitlab.io/martian-bots/)

[CI](https://gitlab.com/mihirk/martian-bots/pipelines)


# Setup
Make sure you have the latest node and npm installed
From the project directory run 

`make install`

# Development
* Run Test - `make test`
* Run Dev Server - `make run`
* Production Build - `make build`
* Run Build Server - `make run-build`

# Build
The CI Builds the project and pushes it to Gitlab pages

[Project Page](https://mihirk.gitlab.io/martian-bots/)
